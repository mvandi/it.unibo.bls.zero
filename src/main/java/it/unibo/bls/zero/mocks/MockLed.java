package it.unibo.bls.zero.mocks;

import it.unibo.bls.zero.interfaces.AbstractLed;

public final class MockLed extends AbstractLed {

	private boolean on;
	
	public MockLed() {
		System.out.println("Mock led created");
	}

	@Override
	public void turnOn() {
		on = true;
		System.out.println("Mock led turned on");
	}

	@Override
	public void turnOff() {
		on = false;
		System.out.println("Mock led turned off");
	}

	@Override
	public boolean isOn() {
		return on;
	}

}
