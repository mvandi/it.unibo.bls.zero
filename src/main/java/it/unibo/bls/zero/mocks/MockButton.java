package it.unibo.bls.zero.mocks;

import it.unibo.bls.zero.interfaces.AbstractObservable;
import it.unibo.bls.zero.interfaces.IMockButton;

public final class MockButton extends AbstractObservable implements IMockButton {
	
	public MockButton() {
		System.out.println("Mock button created");
	}

	@Override
	public void press() {
		System.out.println("Mock button pressed");
		update("pressed");
	}

}
