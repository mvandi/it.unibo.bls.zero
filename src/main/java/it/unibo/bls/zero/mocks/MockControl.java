package it.unibo.bls.zero.mocks;

import java.util.Objects;

import it.unibo.bls.zero.interfaces.IControl;
import it.unibo.bls.zero.interfaces.ILed;

public final class MockControl implements IControl {

	private final ILed led;

	public MockControl(ILed led) {
		this.led = led;
		System.out.println("Mock control created");
	}

	@Override
	public void update(String state) {
		if ("pressed".equals(state)) {
			led.ledSwitch();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		MockControl other = (MockControl) obj;
		if (led == null) {
			if (other.led != null)
				return false;
		} else if (!Objects.equals(led, other.led))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(led);
	}

}
