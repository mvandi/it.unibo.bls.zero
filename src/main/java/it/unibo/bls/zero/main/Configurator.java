package it.unibo.bls.zero.main;

import it.unibo.bls.zero.interfaces.IControl;
import it.unibo.bls.zero.interfaces.ILed;
import it.unibo.bls.zero.interfaces.IMockButton;
import it.unibo.bls.zero.mocks.MockButton;
import it.unibo.bls.zero.mocks.MockControl;
import it.unibo.bls.zero.mocks.MockLed;

public final class Configurator {

	private static ILed led;
	private static IControl control;
	private static IMockButton button;

	private Configurator() {
	}
	
	public static ILed getLed() {
		return led;
	}

	public static IControl getControl() {
		return control;
	}

	public static IMockButton getButton() {
		return button;
	}

	public static void configureTheSystem() {
		createComponents();
		assemblySystem();
		startComponents();
	}

	public static void tearDownTheSystem() {
		button.removeObserver(control);

		led = null;
		control = null;
		button = null;
	}

	public static void main(final String... args) {
		configureTheSystem();
	}

	private static void createComponents() {
		led = new MockLed();
		control = new MockControl(led);

		button = new MockButton();
	}

	private static void assemblySystem() {
		button.addObserver(control);
	}

	private static void startComponents() {
	}

}
