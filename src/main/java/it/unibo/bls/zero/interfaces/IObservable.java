package it.unibo.bls.zero.interfaces;

public interface IObservable {

	void addObserver(IObserver observer);

	void removeObserver(IObserver observer);

}
