package it.unibo.bls.zero.interfaces;

public abstract class AbstractLed implements ILed {

	protected AbstractLed() {
	}

	@Override
	public final void ledSwitch() {
		if (isOn()) {
			turnOff();
		} else {
			turnOn();
		}
	}

}
