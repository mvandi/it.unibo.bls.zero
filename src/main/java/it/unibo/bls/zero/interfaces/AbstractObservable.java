package it.unibo.bls.zero.interfaces;

import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Supplier;

public abstract class AbstractObservable implements IObservable {

	private final Collection<IObserver> observers;

	protected AbstractObservable() {
		this(LinkedList::new);
	}

	protected AbstractObservable(Supplier<? extends Collection<IObserver>> collectionSuppplier) {
		observers = collectionSuppplier.get();
	}

	@Override
	public final void addObserver(final IObserver observer) {
		observers.add(observer);
	}

	@Override
	public final void removeObserver(final IObserver observer) {
		observers.remove(observer);
	}

	protected final void update(final String state) {
		observers.forEach(observer -> observer.update(state));
	}

}
