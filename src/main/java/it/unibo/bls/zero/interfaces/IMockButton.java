package it.unibo.bls.zero.interfaces;

public interface IMockButton extends IButton {

	void press();

}
