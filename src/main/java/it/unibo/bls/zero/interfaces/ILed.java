package it.unibo.bls.zero.interfaces;

public interface ILed {

	boolean isOn();

	void turnOn();

	void turnOff();

	void ledSwitch();

}
