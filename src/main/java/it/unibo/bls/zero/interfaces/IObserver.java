package it.unibo.bls.zero.interfaces;

public interface IObserver {

	void update(String state);

}
