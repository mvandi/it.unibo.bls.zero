package it.unibo.bls.zero.integration_tests;

import it.unibo.bls.zero.main.Configurator;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControlTest {

	@Before
	public void setUp() {
		Configurator.configureTheSystem();
	}
	
	@After
	public void tearDown() {
	}

	@Test
	public void testCreation() {
		System.out.println("[control]: Test creation");

		assertFalse(Configurator.getLed().isOn());
	}

	@Test
	public void testPress() {
		System.out.println("[contro]: Test press button");

		Configurator.getButton().press();
		assertTrue(Configurator.getLed().isOn());
	}

	@Test
	public void testPressTwice() {
		System.out.println("[contro]: Test press button twice");

		Configurator.getButton().press();
		Configurator.getButton().press();
		assertFalse(Configurator.getLed().isOn());
	}

}
