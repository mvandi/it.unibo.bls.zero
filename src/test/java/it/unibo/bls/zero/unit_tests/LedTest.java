package it.unibo.bls.zero.unit_tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import it.unibo.bls.zero.interfaces.ILed;
import it.unibo.bls.zero.mocks.MockLed;

public class LedTest {
	
	private ILed led;
	
	@Before
	public void setUp() {
		led = new MockLed();
	}

	@Test
	public void testCreation() {
		System.out.println("[led]: Test creation");

		assertFalse(led.isOn());
	}

	@Test
	public void testTurnOn() {
		System.out.println("[led]: Test turn on");

		led.turnOn();
		assertTrue(led.isOn());
	}

	@Test
	public void testTurnOff() {
		System.out.println("[led]: Test turn off");

		led.turnOn();
		assertTrue(led.isOn());

		led.turnOff();
		assertFalse(led.isOn());
	}

}
