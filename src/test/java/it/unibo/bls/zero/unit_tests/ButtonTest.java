package it.unibo.bls.zero.unit_tests;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.unibo.bls.zero.interfaces.IMockButton;
import it.unibo.bls.zero.interfaces.IObserver;
import it.unibo.bls.zero.mocks.MockButton;

public class ButtonTest {
	
	private IMockButton button;
	private IObserver observer;

	@Before
	public void setUp() {
		button = new MockButton();
		observer = mock(IObserver.class);
		button.addObserver(observer);
	}

	@After
	public void tearDown() {
		button.removeObserver(observer);
	}

	@Test
	public void testCreation() {
		System.out.println("[button]: Test creation");

		verify(observer, never()).update(anyString());
	}

	@Test
	public void testPress() {
		System.out.println("[button]: Test press");

		button.press();
		verify(observer).update(eq("pressed"));
	}

}
